function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
}

let xs = R.reverse(R.range(0, 50));

function inr (n, x) {
  if (x > 0) return x - floor(x / n) * n
  if (x < 0) return x + ceil(x / n) * n
}

function draw() {

  colorMode(HSB)
  background(0, 0, 0);

  // rotateX(PI * r);

  const r = frameCount * 0.01;
  const sinR = sin(r) / tan(r);
  const lfo = sin(r / 100);
  rotateY(r)

  noFill()
  strokeWeight(1)

  xs.forEach((j, i) => {
    const I = pow(1.13, i);

    const s = I + 5;
    const ry = exp(1.4, i) / 20;
    const h = j + r * 8 * (lfo + 1);

    const l = 130 + (cos(j / 20 + r / 2) + cos(j / 8 + r * 10) / 2) * 125;
    const c = color(inr(255, h), 100, l);

    if (s < windowHeight * 0.5) {
      stroke(c)
      rotateY(ry / 10 * sin(r));
      rotateZ(sinR * s / 200);
      box(s, s, s, 4, 4);
    }
  });
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
